package com.nerds.stocks.job;

import org.apache.commons.collections4.CollectionUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.data.Quote;
import com.nerds.stocks.db.entities.RecentQuoteEntity;
import com.nerds.stocks.db.entities.StockEntity;
import com.nerds.stocks.db.repos.StocksRepository;
import com.nerds.stocks.service.api.ServiceProvider;

@Component
public class StocksInitializer {
    @Autowired
    ServiceProvider api;
    @Autowired
    StocksRepository stocksRepository;
    @Autowired
    PriceUpdater priceUpdater;

    /**
     * Returns an entity instance of the initialized stock. If the current quotes is
     * given as null, this will search and update the database with recent quote
     */
    public Fundamentals initStock(String exchange, String ticker, Quote currentPrice) {
        Fundamentals fundamentals = api.getFundamentals(exchange, ticker);
        if (currentPrice == null) {
            currentPrice = api.getCurrentPrice(exchange, ticker);
        }
        fundamentals.setCurrentPrice(currentPrice);

        if (fundamentals.getStats() == null) {
            fundamentals.setStats(api.getStats(exchange, ticker));
        }

        if (CollectionUtils.isEmpty(fundamentals.getFinancials())) {
            fundamentals.setFinancials(api.getFinancials(exchange, ticker));
        }

        return fundamentals;
    }

    @Async
    public void saveStockDetailsToDb(Fundamentals fundamentals) {
        StockEntity entity = null;
        try {
            entity = fundamentals.toStockEntity();
            if (fundamentals.getCurrentPrice() != null) {
                RecentQuoteEntity recentQuote = fundamentals.getCurrentPrice().toRecentQuoteEntity(entity);
                entity.setCurrentPrice(recentQuote);
            }
            entity = stocksRepository.save(entity);
        } catch (Exception e) {
            logger.error("Error while trying to save the entity fundamentals", e);
        }

        if (entity != null) {
            priceUpdater.initPrices(entity);
        }
    }

    static final Logger logger = LoggerFactory.getLogger(StocksInitializer.class);
}