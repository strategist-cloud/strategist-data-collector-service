package com.nerds.stocks.job;

import java.time.Duration;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Arrays;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;

import com.nerds.stocks.db.repos.StocksRepository;
import com.nerds.stocks.service.api.ServiceProvider;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.concurrent.ConcurrentTaskScheduler;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
@ConfigurationProperties(prefix = "stocks")
public class StartUpJob {

    String defaultTickers;

    @Autowired
    StocksInitializer stocksLoader;

    @Autowired
    StocksRepository stocksRepository;

    @Autowired
    ServiceProvider api;

    @Autowired
    PriceUpdater quotesUpdateService;

    // @Bean
    ApplicationRunner init() {
        return args -> {
            loadInitialStocksData();
        };
    }

    Runnable initDefaultStocksDataRunnable = new Runnable() {
        @Override
        public void run() {
            loadInitialStocksData();
        }
    };

    // @PostConstruct
    @EventListener(ApplicationReadyEvent.class)
    @Async
    public void initLoader() {
        TaskScheduler scheduler;
        ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
        scheduler = new ConcurrentTaskScheduler(localExecutor);
        scheduler.schedule(initDefaultStocksDataRunnable,
                LocalDateTime.now().plusSeconds(15).toInstant(ZoneOffset.UTC)); // start the job in 15 seconds after the
                                                                                // server is loaded

        // Let's not hit periodically in development mode
        updateQuotesPeriodically();
    }

    public void loadInitialStocksData() {
        logger.info("***************Starting the initial loader.");
        if (!StringUtils.isEmpty(defaultTickers)) {
            logger.info("Found Initial tickers as " + defaultTickers);

            Arrays.asList(defaultTickers.split(",")).stream().map(exchangeTickerMap -> exchangeTickerMap.split(":"))
                    .filter(exchangeTickerMap -> exchangeTickerMap.length == 2)
                    .filter(exchangeTickerMap -> stocksRepository.get(exchangeTickerMap[0],
                            exchangeTickerMap[1]) == null)
                    .forEach(exchangeTickerMap -> {
                        logger.info("Started loading info for " + exchangeTickerMap[1] + " in " + exchangeTickerMap[0]
                                + " exchange.");

                        stocksLoader.initStock(exchangeTickerMap[0], exchangeTickerMap[1], null);
                        logger.info("Completed loading info for " + exchangeTickerMap[1]);
                    });

            initiateSentimentsCache();

        } else {
            logger.error("No Default Tickers found");
        }

        logger.info("********************Completed loading initial stocks");
    }

    private void initiateSentimentsCache() {

    }

    public String getDefaultTickers() {
        return this.defaultTickers;
    }

    public void setDefaultTickers(String defaultTickers) {
        this.defaultTickers = defaultTickers;
    }

    public void updateQuotesPeriodically() {
        LocalDateTime localDateTime = LocalDateTime.now();
        localDateTime = localDateTime.minusSeconds(localDateTime.getSecond()) // remove seconds
                .plusMinutes(5 - (localDateTime.getMinute() % 5)); // add minutes so that the result minute will be a
                                                                   // multiple of 5
        ScheduledExecutorService localExecutor = Executors.newSingleThreadScheduledExecutor();
        TaskScheduler scheduler = new ConcurrentTaskScheduler(localExecutor);
        scheduler.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                quotesUpdateService.updateFiveMin(SCHEDULE_JOB);
            }
        }, localDateTime.toInstant(ZoneOffset.UTC), Duration.ofMinutes(5));

        localDateTime = LocalDateTime.now();
        localDateTime = localDateTime.minusSeconds(localDateTime.getSecond()) // remove seconds
                .plusMinutes(30 - (localDateTime.getMinute() % 30)); // add minutes so that the result minute will be a
                                                                     // multiple of 30
        ScheduledExecutorService localExecutor1 = Executors.newSingleThreadScheduledExecutor();
        TaskScheduler scheduler1 = new ConcurrentTaskScheduler(localExecutor1);
        scheduler1.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                quotesUpdateService.updateThirtyMin(SCHEDULE_JOB);
            }
        }, localDateTime.toInstant(ZoneOffset.UTC), Duration.ofMinutes(30));

        localDateTime = LocalDateTime.now();
        localDateTime = localDateTime.minusHours(localDateTime.getHour()) // remove hours
                .minusSeconds(localDateTime.getSecond()) // remove seconds
                .minusMinutes(localDateTime.getMinute()) // remove minutes
                .plusDays(1);
        ScheduledExecutorService localExecutor2 = Executors.newSingleThreadScheduledExecutor();
        TaskScheduler scheduler2 = new ConcurrentTaskScheduler(localExecutor2);
        scheduler2.scheduleWithFixedDelay(new Runnable() {
            @Override
            public void run() {
                quotesUpdateService.updateDaily(SCHEDULE_JOB);
            }
        }, localDateTime.toInstant(ZoneOffset.UTC), Duration.ofDays(1));
    }

    static final String SCHEDULE_JOB = "SCHEDULER";
    static final Logger logger = LoggerFactory.getLogger(StartUpJob.class);

}