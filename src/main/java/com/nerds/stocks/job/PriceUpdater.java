package com.nerds.stocks.job;

import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import com.nerds.stocks.data.Quote;
import com.nerds.stocks.db.entities.DailyQuoteEntity;
import com.nerds.stocks.db.entities.FiveMinQuoteEntity;
import com.nerds.stocks.db.entities.JobsEntity;
import com.nerds.stocks.db.entities.RecentQuoteEntity;
import com.nerds.stocks.db.entities.StockEntity;
import com.nerds.stocks.db.entities.ThirtyMinQuoteEntity;
import com.nerds.stocks.db.repos.DailyQuotesRepository;
import com.nerds.stocks.db.repos.FiveMinQuotesRepository;
import com.nerds.stocks.db.repos.JobsRepository;
import com.nerds.stocks.db.repos.JobsRepository.JOB_NAMES;
import com.nerds.stocks.db.repos.QuotesRepository;
import com.nerds.stocks.db.repos.StocksRepository;
import com.nerds.stocks.db.repos.ThirtyMinQuotesRepository;
import com.nerds.stocks.service.api.ServiceProvider;
import com.nerds.stocks.service.data.INTERVAL;
import com.nerds.stocks.util.DateUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class PriceUpdater {

    @Autowired
    QuotesRepository recentQuotesRepository;
    @Autowired
    FiveMinQuotesRepository fiveMinQuotesRepository;
    @Autowired
    ThirtyMinQuotesRepository thirtyMinQuotesRepository;
    @Autowired
    DailyQuotesRepository dailyQuotesRepository;
    @Autowired
    StocksRepository stocksRepository;
    @Autowired
    JobsRepository jobsRepository;
    @Autowired
    ServiceProvider api;

    final List<INTERVAL> tasks = Arrays.asList(INTERVAL.values());

    @Async
    public void currentPriceUpdate() {
        logger.info("*****************Starting current prices Updater Job ");

        List<RecentQuoteEntity> recentQuotesFromDb = recentQuotesRepository.getLatestRecords();

        List<RecentQuoteEntity> entities = recentQuotesFromDb.parallelStream().map(recentQuoteInDb -> {
            StockEntity stockInfo = recentQuoteInDb.getStock();
            Quote quote = api.getCurrentPrice(stockInfo.getExchange(), stockInfo.getSymbol());
            return quote.toRecentQuoteEntity(stockInfo);
        }).collect(Collectors.toList());

        recentQuotesRepository.saveAll(entities);
        recentQuotesRepository.flush();
    }

    @Async
    public void updateFiveMin(String programName) {
        LocalDateTime cutOffTime = LocalDateTime.now().minusDays(5);

        logger.info("*****************Starting FIVE MIN Prices Update Job from " + cutOffTime);
        updateJobInfo(JOB_NAMES.FIVE_MIN_QUOTE_UPDATE, programName, false);

        List<FiveMinQuoteEntity> entities = new ArrayList<FiveMinQuoteEntity>();
        fiveMinQuotesRepository.getLatestRecords().stream().forEach(quoteEntity -> {
            Timestamp lastUpdatedTimestamp = quoteEntity.getEndTime();

            LocalDateTime fromTime;
            if (lastUpdatedTimestamp == null || lastUpdatedTimestamp.toLocalDateTime().isBefore(cutOffTime)) {
                fromTime = cutOffTime;
            } else {
                fromTime = lastUpdatedTimestamp.toLocalDateTime();
            }

            // Get the Prices from the last n days
            List<Quote> quotes = api.getPricesFrom(quoteEntity.getStock().getExchange(),
                    quoteEntity.getStock().getSymbol(), fromTime, INTERVAL._5MIN);
            entities.addAll(quotes.parallelStream().map(quote -> quote.toFiveMinQuoteEntity(quoteEntity.getStock()))
                    .collect(Collectors.toList()));

        });

        fiveMinQuotesRepository.saveAll(entities);
        fiveMinQuotesRepository.flush();
        updateJobInfo(JOB_NAMES.FIVE_MIN_QUOTE_UPDATE, programName, true);
    }

    @Async
    public void updateThirtyMin(String programName) {
        logger.info("*****************Starting THIRTY MIN Prices Update Job");
        updateJobInfo(JOB_NAMES.THIRTY_MIN_QUOTE_UPDATE, programName, false);
        List<ThirtyMinQuoteEntity> entities = new ArrayList<ThirtyMinQuoteEntity>();
        LocalDateTime cutOffTime = LocalDateTime.now();
        cutOffTime.minusMonths(1);

        thirtyMinQuotesRepository.getLatestRecords().stream().forEach(quoteEntity -> {
            Timestamp lastUpdatedTimestamp = quoteEntity.getEndTime();
            LocalDateTime fromTime;
            if (lastUpdatedTimestamp == null || lastUpdatedTimestamp.toLocalDateTime().isBefore(cutOffTime)) {
                fromTime = cutOffTime;
            } else {
                fromTime = lastUpdatedTimestamp.toLocalDateTime();
            }

            List<Quote> quotes = api.getPricesFrom(quoteEntity.getStock().getExchange(),
                    quoteEntity.getStock().getSymbol(), fromTime, INTERVAL._30MIN);
            entities.addAll(quotes.stream().map(quote -> quote.toThirtyMinQuoteEntity(quoteEntity.getStock()))
                    .collect(Collectors.toList()));
        });

        thirtyMinQuotesRepository.saveAll(entities);
        thirtyMinQuotesRepository.flush();
        updateJobInfo(JOB_NAMES.THIRTY_MIN_QUOTE_UPDATE, programName, true);
    }

    @Async
    public void updateDaily(String programName) {
        logger.info("*****************Starting Daily Prices Update Job");
        updateJobInfo(JOB_NAMES.DAILY_QUOTE_UPDATE, programName, false);
        LocalDateTime cutOffTime = LocalDateTime.now();
        cutOffTime.minusYears(10);

        List<DailyQuoteEntity> entities = new ArrayList<DailyQuoteEntity>();
        dailyQuotesRepository.getLatestRecords().parallelStream().forEach(quoteEntity -> {
            Timestamp lastUpdatedTimestamp = quoteEntity.getEndTime();
            LocalDateTime fromTime;
            if (lastUpdatedTimestamp == null || lastUpdatedTimestamp.toLocalDateTime().isBefore(cutOffTime)) {
                fromTime = cutOffTime;
            } else {
                fromTime = lastUpdatedTimestamp.toLocalDateTime();
            }

            List<Quote> quotes = api.getPricesFrom(quoteEntity.getStock().getExchange(),
                    quoteEntity.getStock().getSymbol(), fromTime, INTERVAL.DAILY);
            entities.addAll(quotes.parallelStream().map(quote -> quote.toDailyQuoteEntity(quoteEntity.getStock()))
                    .collect(Collectors.toList()));
        });

        dailyQuotesRepository.saveAll(entities);
        dailyQuotesRepository.flush();
        updateJobInfo(JOB_NAMES.DAILY_QUOTE_UPDATE, programName, true);
    }

    @Async
    public void initPrices(final StockEntity stock) {
        String ticker = stock.getSymbol();
        tasks.stream().forEach(task -> {
            // TimeUtil.getTimeseries(task);
            if (task == INTERVAL._5MIN) {
                List<Quote> fiveMinQuotes = api.getPrices(stock.getExchange(), ticker, task);
                fiveMinQuotesRepository.saveAll(fiveMinQuotes.parallelStream()
                        .map(quote -> quote.toFiveMinQuoteEntity(stock)).collect(Collectors.toList()));
                fiveMinQuotesRepository.flush();
            } else if (task == INTERVAL._30MIN) {
                List<Quote> thirtyMinQuotes = api.getPrices(stock.getExchange(), ticker, task);
                thirtyMinQuotesRepository.saveAll(thirtyMinQuotes.parallelStream()
                        .map(quote -> quote.toThirtyMinQuoteEntity(stock)).collect(Collectors.toList()));
                thirtyMinQuotesRepository.flush();
            } else if (task == INTERVAL.DAILY) {
                List<Quote> tenYearquotes = api.getPrices(stock.getExchange(), ticker, task);
                dailyQuotesRepository.saveAll(tenYearquotes.parallelStream()
                        .map(quote -> quote.toDailyQuoteEntity(stock)).collect(Collectors.toList()));
                dailyQuotesRepository.flush();
            } else {
                logger.info("Skipping weekly repository maintenance for now");
            }
        });
    }

    private void updateJobInfo(JOB_NAMES jobName, String programName, boolean isJobCompleted) {
        try {
            Optional<JobsEntity> job = jobsRepository.findById(jobName.getName());
            JobsEntity entity = job.orElse(new JobsEntity());

            entity.setName(jobName.getName());
            if (isJobCompleted) {
                entity.setLastRunEndTime(DateUtil.getTimestamp(LocalDateTime.now()));
            } else {
                entity.setLastRunStartTime(DateUtil.getTimestamp(LocalDateTime.now()));
                entity.setLastRunEndTime(null);
            }
            entity.setInitiatedBy(programName);
            jobsRepository.saveAndFlush(entity);
        } catch (Exception e) {
            logger.error("Error while trying to update Jobs repository for " + jobName, e);
        }

    }

    static final Logger logger = LoggerFactory.getLogger(PriceUpdater.class);

}