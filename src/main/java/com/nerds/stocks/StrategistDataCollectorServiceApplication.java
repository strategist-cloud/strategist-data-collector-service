package com.nerds.stocks;

import com.nerds.stocks.config.FeignClientInterceptor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.annotation.EnableScheduling;

import feign.RequestInterceptor;

@SpringBootApplication
@EnableJpaRepositories
@EnableScheduling
@EnableAsync
@EnableConfigurationProperties
@EnableAutoConfiguration
@EnableFeignClients
public class StrategistDataCollectorServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(StrategistDataCollectorServiceApplication.class, args);
	}

	@Bean
	public RequestInterceptor getUserFeignClientInterceptor() {
		return new FeignClientInterceptor();
	}
}
