package com.nerds.stocks.controller;

import static org.springframework.http.HttpStatus.OK;

import java.util.List;
import java.util.stream.Collectors;

import com.nerds.stocks.data.Exchange;
import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.db.repos.ExchangeRepository;
import com.nerds.stocks.service.api.ServiceProvider;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/exchanges")
public class ExchangeController {

	@Autowired
	ExchangeRepository exchangesRepository;

	@Autowired
    ServiceProvider api;
	
	@RequestMapping("/getAll")
	public List<Exchange> getAll() {
		return exchangesRepository.findAll().parallelStream()
				.map(exchangeEntity -> new Exchange(exchangeEntity))
				.collect(Collectors.toList());
		
	}

	@RequestMapping(path = "/searchSymbols/{searchstr}")
    public ResponseEntity<List<Fundamentals>> searchSymbols(String searchstr) {
        return new ResponseEntity<List<Fundamentals>>(api.searchTickers(searchstr), OK);
    }
}
