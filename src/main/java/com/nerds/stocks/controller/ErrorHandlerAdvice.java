package com.nerds.stocks.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.nerds.stocks.controller.response.ResourceException;

@ControllerAdvice
public class ErrorHandlerAdvice {

    @ExceptionHandler(ResourceException.class)
    public ResponseEntity<String> handleException(ResourceException e) {
        return ResponseEntity.status(e.getHttpStatus()).body(e.getMessage());
    }        
}