package com.nerds.stocks.controller;

import java.security.Principal;
import java.util.List;

import com.nerds.stocks.data.Fundamentals;
import com.nerds.stocks.db.entities.JobsEntity;
import com.nerds.stocks.db.repos.JobsRepository;
import com.nerds.stocks.job.PriceUpdater;
import com.nerds.stocks.job.StocksInitializer;
import com.nerds.stocks.service.cache.ExchangeCache;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/dataCollectorService")
public class PriceController {

    @Autowired
    PriceUpdater priceUpdater;

    @Autowired
    JobsRepository jobsRepository;

    @Autowired
    ExchangeCache exchangesCache;

    @Autowired
    StocksInitializer initializer;

    @RequestMapping(path = "/refreshAllQuotes")
    public ResponseEntity<String> updatePrices(Principal principal) {
        String userName = principal.getName();
        if (StringUtils.isEmpty(userName)) {
            return new ResponseEntity<String>("Try again after signing in", HttpStatus.UNAUTHORIZED);
        } else {
            priceUpdater.currentPriceUpdate();
            priceUpdater.updateFiveMin(userName);
            priceUpdater.updateThirtyMin(userName);
            priceUpdater.updateDaily(userName);

            return new ResponseEntity<String>("Jobs started", HttpStatus.OK);
        }
    }

    @RequestMapping(path = "/refreshQuotes/{jobName}")
    public ResponseEntity<String> updatePrices(Principal principal, @PathVariable("jobName") String givenJobName) {
        String userName = principal.getName();
        if (StringUtils.isEmpty(userName)) {
            return new ResponseEntity<String>("Try again after signing in", HttpStatus.UNAUTHORIZED);
        } else {
            JobsRepository.JOB_NAMES jobName = JobsRepository.JOB_NAMES.valueOf(givenJobName);
            if (jobName == null) {
                return new ResponseEntity<String>("Invalid Job Name", HttpStatus.OK);
            } else if (jobName == JobsRepository.JOB_NAMES.FIVE_MIN_QUOTE_UPDATE) {
                priceUpdater.updateFiveMin(userName);
            } else if (jobName == JobsRepository.JOB_NAMES.THIRTY_MIN_QUOTE_UPDATE) {
                priceUpdater.updateThirtyMin(userName);
            } else if (jobName == JobsRepository.JOB_NAMES.DAILY_QUOTE_UPDATE) {
                priceUpdater.updateDaily(userName);
            } else {
                priceUpdater.currentPriceUpdate();
            }

            return new ResponseEntity<String>("Jobs started", HttpStatus.OK);
        }
    }

    @RequestMapping(path = "/getAllJobs")
    public ResponseEntity<List<JobsEntity>> getAllJobs(Principal principal) {
        List<JobsEntity> jobs = jobsRepository.findAll(Sort.by(Direction.DESC, "lastRunStartTime"));
        return new ResponseEntity<List<JobsEntity>>(jobs, HttpStatus.OK);
    }

    @RequestMapping("/initStock/{exchange}/{ticker}")
    public ResponseEntity<Fundamentals> initStock(@PathVariable String exchange, @PathVariable String ticker) {
        Fundamentals stock = initializer.initStock(exchange, ticker, null);
        if (stock == null) {
            stock = new Fundamentals();
        } else {
            initializer.saveStockDetailsToDb(stock);
        }

        return new ResponseEntity<Fundamentals>(stock, HttpStatus.OK);
    }

    static final Logger logger = LoggerFactory.getLogger(PriceController.class);
}