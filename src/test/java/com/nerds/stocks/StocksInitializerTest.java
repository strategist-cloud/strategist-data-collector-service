package com.nerds.stocks;

import com.nerds.stocks.config.StrategistExternalServicesConfig;
import com.nerds.stocks.job.StocksInitializer;
import com.nerds.stocks.service.cache.ExchangeMap;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ComponentScan(basePackages = "com.nerds.stocks")
@ContextConfiguration(classes = { StrategistDataAcessApplication.class })
@SpringBootTest(classes = { StrategistExternalServicesConfig.class, ExchangeMap.class })
public class StocksInitializerTest {

    @Autowired
    StocksInitializer initializer;

    @Test
    public void testInitStock() {
        initializer.initStock("NYSE", "MI", null);

    }

}